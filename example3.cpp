//#include <opencv2/opencv.hpp>
#include "highgui.h"
#include "opencv2/imgproc/imgproc_c.h"


void saturate_sv(IplImage *img)
{
	for(int y=0; y<img->height; y++)
	{
		uchar *ptr=(uchar *)(img->imageData+y*img->widthStep);
		for(int x=0; x<img->width; x++)
		{
			ptr[3*x+1] = 125;
			ptr[3*x+2] = 100;
		}
	}
}

int main( int argc, char **argv )
{
    IplImage* img = cvLoadImage( argv[1] ), *img2; 
    cvNamedWindow( "Example1", 0 ); 
    cvNamedWindow( "Example2", 0 ); 

    saturate_sv(img);

    cvShowImage( "Example1", img );
    cvPyrDown( img, img2 );
    cvShowImage( "Example2", img2 );
    cvWaitKey(0);
    return 0;
};

