#include <iostream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp> // imread, imshow, videocapture, waitkey
#include <opencv2/nonfree/gpu.hpp> // SURF_GPU
#include <opencv2/calib3d/calib3d.hpp>

using namespace std;
using namespace cv;
using namespace gpu;


int main()
{
    Mat img = imread("image.jpg", CV_LOAD_IMAGE_GRAYSCALE); // Loads the image into the img object
    VideoCapture cap(0); // The video object that will get the image from the webcam
    Mat frame; // The individual frame from the video
    Mat window; // Object that will serve as window

    GpuMat img_gpu, vid_gpu; // The image and video GPU objects
    GpuMat imgkey_gpu, vidkey_gpu; // The image and video GPU keyframe objects
    GpuMat imgdes_gpu, viddes_gpu; // The image and video GPu descriptors
    gpu::SURF_GPU surf(500); // Declares a surf object


    vector<cv::KeyPoint> imgkey_vec, vidkey_vec; // The vector for the image and video keypoints
    vector<float> imgdes_vec, viddes_vec; // The vector for the image and video descriptors


    img_gpu.upload(img);
    surf(img_gpu, GpuMat(), imgkey_gpu, imgdes_gpu, false);
    surf.downloadKeypoints(imgkey_gpu, imgkey_vec);
    surf.downloadDescriptors(imgdes_gpu, imgdes_vec);


    BruteForceMatcher_GPU<L2<float>> matcher;


    vector<Point2f> img_corners(4);
    img_corners[0] = cvPoint(0, 0);
    img_corners[1] = cvPoint( img.cols, 0 );
    img_corners[2] = cvPoint( img.cols, img.rows );
    img_corners[3] = cvPoint( 0, img.rows );

    double max_dist = 0; double min_dist = 100;


    while (1)
    {
        cap >> frame; // Puts individual video frame into the 'frame' object
        Mat bwframe;
        cvtColor(frame, bwframe, CV_RGB2GRAY);
        vid_gpu.upload(bwframe);

        surf(vid_gpu, GpuMat(), vidkey_gpu, viddes_gpu, false);
        surf.downloadKeypoints(vidkey_gpu, vidkey_vec);
        surf.downloadDescriptors(viddes_gpu, viddes_vec);

        vector< vector<DMatch> > matches;
        vector< DMatch > good_matches;

        matcher.knnMatch(imgdes_gpu, viddes_gpu, matches, 3);
        double tresholdDist = 0.25 * sqrt(double(bwframe.size().height * bwframe.size().height + bwframe.size().width * bwframe.size().width));

        good_matches.reserve(matches.size());
        for (size_t i = 0; i < matches.size(); ++i)
        {
            for (int j = 0; j < matches[i].size(); j++)
            {
                Point2f from = imgkey_vec[matches[i][j].queryIdx].pt;
                Point2f to = vidkey_vec[matches[i][j].trainIdx].pt;
                double dist = sqrt((from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y));

                if (dist < tresholdDist)
                {
                    good_matches.push_back(matches[i][j]);
                    j = matches[i].size();
                }
            }
        }

        drawMatches(img, imgkey_vec, frame, vidkey_vec, good_matches, window);

        vector<Point2f> imgpoint, vidpoint;
        for (int i = 0; i < good_matches.size(); i++)
        {
            imgpoint.push_back(imgkey_vec[good_matches[i].queryIdx].pt);
            vidpoint.push_back(vidkey_vec[good_matches[i].trainIdx].pt);
        }


        Mat H = findHomography(imgpoint, vidpoint, CV_RANSAC);

        vector<Point2f> vid_corners(4);
        perspectiveTransform(img_corners, vid_corners, H);

        line( window, vid_corners[0] + Point2f( img.cols, 0), vid_corners[1] + Point2f( img.cols, 0), Scalar(0, 255, 0), 4 );
        line( window, vid_corners[1] + Point2f( img.cols, 0), vid_corners[2] + Point2f( img.cols, 0), Scalar( 0, 255, 0), 4 );
        line( window, vid_corners[2] + Point2f( img.cols, 0), vid_corners[3] + Point2f( img.cols, 0), Scalar( 0, 255, 0), 4 );
        line( window, vid_corners[3] + Point2f( img.cols, 0), vid_corners[0] + Point2f( img.cols, 0), Scalar( 0, 255, 0), 4 );

        imshow("window", window);
        if (waitKey(30) > 0)break;
    }
}