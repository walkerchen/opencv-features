#include <opencv2/opencv.hpp>
#include <opencv2/core/types_c.h>

using namespace cv;

void func1()
{
    Mat m = Mat::eye( 10, 10, cv::DataType<cv::Complexf>::type );
    //printf("Element (3, 3) is %f + i%f\n", m.at<cv::Complexf>(3, 3).re, m.at<cv::Complexf>(3, 3).im);
    printf("size of mat: (%d,%d)\n", m.size().width, m.size().height);
}

void func2(char **argv)
{
    IplImage *img = cvLoadImage(argv[1]);
    cvNamedWindow(argv[0], 0);
    //cvFlip(img, NULL, 1);
    //cvZero(img);
    cvSave("img1.xml", img);
    cvShowImage(argv[0], img);
    cvWaitKey(0);
    cvReleaseImage(&img);
    cvDestroyWindow(argv[0]);
}

void func3(char **argv)
{
    IplImage *img = cvLoadImage(argv[1]);
    IplImage *dst = cvCloneImage(img);
    IplImage *dst2 = cvCloneImage(img);
    cvErode(img, dst, NULL, 5);
    cvDilate(img, dst2, NULL, 5);
    cvNamedWindow(argv[0], 0);
    cvNamedWindow("dst", 0);
    cvNamedWindow("dst2", 0);
    cvShowImage(argv[0], img);
    cvShowImage("dst", dst);
    cvShowImage("dst2", dst2);
    cvWaitKey(0);
    cvReleaseImage(&img);
    cvReleaseImage(&dst);
    cvDestroyWindow(argv[0]);
}

void func4(char **argv)
{
    IplImage *img = cvLoadImage(argv[1]);
    IplImage *dst = cvCloneImage(img);
    IplImage *tmp = cvCloneImage(img);
    cvMorphologyEx(img, dst, tmp, NULL, CV_MOP_GRADIENT);
    cvNamedWindow(argv[0], 0);
    cvNamedWindow("dst", 0);
    cvShowImage(argv[0], img);
    cvShowImage("dst", dst);
    cvWaitKey(0);
    cvReleaseImage(&img);
    cvReleaseImage(&dst);
    cvDestroyWindow(argv[0]);
}

void func5(char **argv)
{
    IplImage *img = cvLoadImage(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    IplImage *dst = cvCloneImage(img);
    cvAdaptiveThreshold(img, dst, 255);
    cvNamedWindow(argv[0], 0);
    cvNamedWindow("dst", 0);
    cvShowImage(argv[0], img);
    cvShowImage("dst", dst);
    cvWaitKey(0);
    cvReleaseImage(&img);
    cvReleaseImage(&dst);
    cvDestroyWindow(argv[0]);
}

void func6(char **argv)
{
    IplImage *img = cvLoadImage(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    IplImage *dst = cvCloneImage(img);
    float vals[9] = {-100.5, 120.2, 71.3, -90.1, 100.8, -80.3, 120.6, 90.2, -180.2};
    CvMat* mat = cvCreateMat( 3, 3, CV_32FC1 );
    cvInitMatHeader(mat, 3, 3, CV_32FC1, vals);

    cvFilter2D(img, dst, mat);

    cvNamedWindow(argv[0], 0);
    cvNamedWindow("dst", 0);
    cvShowImage(argv[0], img);
    cvShowImage("dst", dst);
    cvWaitKey(0);
    cvReleaseImage(&img);
    cvReleaseImage(&dst);
    cvDestroyWindow(argv[0]);
}

void func7(char **argv)
{
    IplImage *img = cvLoadImage(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    IplImage *dst = cvCloneImage(img);

    cvEqualizeHist(img, dst);

    cvNamedWindow(argv[0], 0);
    cvNamedWindow("dst", 0);
    cvShowImage(argv[0], img);
    cvShowImage("dst", dst);
    cvWaitKey(0);
    cvReleaseImage(&img);
    cvReleaseImage(&dst);
    cvDestroyWindow(argv[0]);
}

int main( int argc, char **argv )
{
    func7(argv);
}