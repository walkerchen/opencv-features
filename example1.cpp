#include "highgui.h"

int main(int argc, char **argv)
{
	IplImage *img = cvLoadImage(argv[1]);
	cvNamedWindow(argv[0], 0);
	cvShowImage(argv[0], img);
	cvWaitKey(0);
	cvReleaseImage(&img);
	cvDestroyWindow(argv[0]);

	return(0);
}