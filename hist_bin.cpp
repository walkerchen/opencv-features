#include <highgui.h>
#include <cv.h>

void hist( IplImage *src )
{
    //src = cvLoadImage(argv[1], 1);
    if ( src  != 0 )
    {
        IplImage *hsv = cvCreateImage( cvGetSize( src ), 8, 3 );
        cvCvtColor( src, hsv, CV_BGR2HSV );  //色彩空间的转换

        //计算直方图，并将其分解为单通道
        IplImage *h_plane = cvCreateImage( cvGetSize( src ), 8, 1 );
        IplImage *s_plane = cvCreateImage( cvGetSize( src ), 8, 1 );
        IplImage *v_plane = cvCreateImage( cvGetSize( src ), 8, 1 );
        IplImage *planes[] = { h_plane, s_plane };
        cvCvtPixToPlane( hsv, h_plane, s_plane, v_plane, 0 );  //分割多通道数组成几个单通道数组或者从数组中提取一个通道,可以看作cvSplit是他的宏

        //构建直方图，并计算直方图
        //int h_bins = 30, s_bins = 20;
        int h_bins = 200, s_bins = 150;
        CvHistogram *hist;
        {
            //单独的模块
            int hist_size[] = { h_bins, s_bins }; //直方图矩阵大小
            float h_ranges[] = { 0, 180 };
            float s_ranges[] = { 0, 255 };
            float *ranges[] = { h_ranges, s_ranges };//二维数组
            hist = cvCreateHist( 2, hist_size, CV_HIST_ARRAY, ranges, 1 ); //创建直方图，参数是维度，矩阵大小，直方图的表示格式（CV_HIST_ARRAY 意味着直方图数据表示为多维密集数组 ），图中方块范围的数组
        };
        cvCalcHist( planes, hist, 0, 0 );  //计算图像image(s) 的直方图，planes是IplImage**类型

        //创建图像来显示直方图
        int scale = 2;
        IplImage *hist_img = cvCreateImage( cvSize( h_bins * scale, s_bins * scale ), 8, 3 );
        cvZero( hist_img );

        //填充
        float max_value = 0;
        cvGetMinMaxHistValue( hist, 0, &max_value, 0 );  //找到最大和最小直方块

        for ( int h = 0; h < h_bins; h++ ) //遍历矩阵
        {
            for ( int s = 0; s < s_bins; s++ )
            {
                float bin_val = cvQueryHistValue_2D( hist, h, s );  //查询二维直方块的值
                int intensity = cvRound( bin_val * 255 );  //将输入浮点数转换为整数
                cvRectangle( hist_img,
                             cvPoint( h * scale, s * scale ),
                             cvPoint( ( ( h + 1 ) * scale - 1 ), ( ( s + 1 ) * scale -  1 ) ),
                             CV_RGB( intensity, intensity, intensity ),
                             CV_FILLED
                           );  //绘制矩形,确定矩形对角线的两个点
            }
        }
        //cvShowImage( "source", src );
        cvShowImage( "H-S Histogram", hist_img );
    }
    else
    {
        printf("Error!\n");
    }
}

int main( int argc, char **argv )
{
    //cv::namedWindow( "Example3", cv::WINDOW_AUTOSIZE );
    cvNamedWindow( "H-S Histogram", 1 );
    cvNamedWindow( "source", 1 );

    CvCapture *capture = 0;
    capture = cvCreateCameraCapture( 0);
    if (!capture)
    {
        return -1;
    }

    IplImage *bgr_frame = cvQueryFrame(capture);

    while ( (bgr_frame = cvQueryFrame(capture)) != NULL )
    {
        //cvShowImage("source", bgr_frame);
        hist(bgr_frame);
        if ( cv::waitKey(33) >= 0 ) break;
    };
    return (0);
}